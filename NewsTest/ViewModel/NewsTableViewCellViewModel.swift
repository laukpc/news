//
//  NewsTableViewCellViewModel.swift
//  NewsTest
//
//  Created by Laurynas Kapacinskas on 2021-08-11.
//

import UIKit

class NewsTableViewCellViewModel {
    let title: String
    let subtitle: String
    let imageURL: URL?
    var imageData: Data? = nil
    
    init(
        title: String,
        subtitle: String,
        imageURL: URL?
        ) {
        self.title = title
        self.subtitle = subtitle
        self.imageURL = imageURL
    }
}
