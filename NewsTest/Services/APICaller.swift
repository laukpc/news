//
//  APICaller.swift
//  NewsTest
//
//  Created by Laurynas Kapacinskas on 2021-08-06.
//

import UIKit

final class APICaller {
        
    static let shared = APICaller()
    
    
        struct Constants {
            static let topHeadlinesURL = URL(string: "https://newsapi.org/v2/top-headlines?country=us&apiKey=686a4fe324bd44f89ec87e0703046585")
        }
    
    
    private init() {}
    
    public func getTopStories(completion: @escaping (Result<[Article], Error>) -> Void) {
        guard let url = Constants.topHeadlinesURL else {
            return
        }
        
        let task = URLSession.shared.dataTask(with: url) { data, _, error in
            if let error = error {
                completion(.failure(error))
            }
            else if let data = data {
                do {
                    let result = try JSONDecoder().decode(APIResponse.self, from: data)
                    completion(.success(result.articles))
                }
                catch {
                    completion(.failure(error))
                }
            }
        }
        task.resume()
    }
    
}

